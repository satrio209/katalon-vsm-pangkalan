package mobile

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable

public class swipe {

	@Keyword
	def SwipeRightAntrean(){

		def slider_width = Mobile.getElementWidth(findTestObject('Object Repository/Antrian Armada, Antrian Penumpang/Antrean Penumpang/Pop Up Ambil No. Antrean/button.SlideAntrean'), 3)
		def x = Mobile.getElementLeftPosition(findTestObject('Object Repository/Antrian Armada, Antrian Penumpang/Antrean Penumpang/Pop Up Ambil No. Antrean/button.SlideAntrean'), 3)
		def y = Mobile.getElementTopPosition(findTestObject('Object Repository/Antrian Armada, Antrian Penumpang/Antrean Penumpang/Pop Up Ambil No. Antrean/button.SlideAntrean'), 3)
		int startX = x + 5
		int startY = y + 5

		int endX = startX + slider_width
		int endY = startY

		Mobile.swipe(startX, startY, endX, endY)
	}

	@Keyword
	def SwipeRightArmada(){

		def slider_width = Mobile.getElementWidth(findTestObject('Object Repository/Antrian Armada, Antrian Penumpang/Antrean Armada/Pop Up Berangkatkan/button.SlideArmada'), 3)
		def x = Mobile.getElementLeftPosition(findTestObject('Object Repository/Antrian Armada, Antrian Penumpang/Antrean Armada/Pop Up Berangkatkan/button.SlideArmada'), 3)
		def y = Mobile.getElementTopPosition(findTestObject('Object Repository/Antrian Armada, Antrian Penumpang/Antrean Armada/Pop Up Berangkatkan/button.SlideArmada'), 3)
		int startX = x + 5
		int startY = y + 5

		int endX = startX + slider_width
		int endY = startY

		Mobile.swipe(startX, startY, endX, endY)
	}
}
