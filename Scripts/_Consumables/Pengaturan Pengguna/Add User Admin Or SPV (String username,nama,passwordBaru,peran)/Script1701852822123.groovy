import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import java.util.concurrent.ConcurrentHashMap.KeySetView as KeySetView
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import io.appium.java_client.AppiumDriver
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import io.appium.java_client.AppiumDriver
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import io.appium.java_client.AppiumDriver
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import io.appium.java_client.AppiumDriver
import com.kms.katalon.core.util.KeywordUtil

String expectedUsername = username

String expectedNama = nama

String expectedPasswordBaru = passwordBaru

def expectedPeran = peran

Mobile.tap(findTestObject('Object Repository/_Global Object/button.Burger'), 0)

Mobile.tap(findTestObject('Object Repository/Burger Menu/button.PengaturanPengguna'), 0)

Mobile.tap(findTestObject('Object Repository/Pengaturan Pengguna Page/button.TambahPengguna'), 0)

Mobile.setText(findTestObject('Object Repository/Pengaturan Pengguna Page/Tambah,Ubah,Delete Pengguna Page/editText.Username'), 
    expectedUsername, 0)

Mobile.setText(findTestObject('Object Repository/Pengaturan Pengguna Page/Tambah,Ubah,Delete Pengguna Page/editText.Nama'), expectedNama, 
    0)

Mobile.setText(findTestObject('Object Repository/Pengaturan Pengguna Page/Tambah,Ubah,Delete Pengguna Page/editText.PasswordBaru'), 
    expectedPasswordBaru, 0)

Mobile.tap(findTestObject('Object Repository/Pengaturan Pengguna Page/Tambah,Ubah,Delete Pengguna Page/Spinner.Peran'), 0)

Mobile.tap(findTestObject('Object Repository/Pengaturan Pengguna Page/Tambah,Ubah,Delete Pengguna Page/spinner.PilihRole',
	[('role') : expectedPeran]), 0)

Mobile.tap(findTestObject('Object Repository/Pengaturan Pengguna Page/Tambah,Ubah,Delete Pengguna Page/button.Simpan'), 0)

//AppiumDriver<?> driver = MobileDriverFactory.getDriver()
//
//def toast = driver.findElementByXPath("//*[(@text = '${user} berhasil dibuat' or . = '${user} berhasil dibuat')]")
//
//if (toast == null) {
//	KeywordUtil.markFailed('ERROR: Toast object not found!')
//}
