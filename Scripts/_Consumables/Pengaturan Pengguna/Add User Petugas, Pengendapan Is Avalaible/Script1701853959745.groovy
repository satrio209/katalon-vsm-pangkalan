import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

String expectedUsername = username

String expectedNama = nama

String expectedPasswordBaru = passwordBaru

def expectedPeran = peran

String expectedCariLokasi = cariLokasi

String expectedLobi = lobi

String expectedPengendapan = pengendapan

Mobile.tap(findTestObject('Object Repository/_Global Object/button.Burger'), 0)

Mobile.tap(findTestObject('Object Repository/Burger Menu/button.PengaturanPengguna'), 0)

Mobile.tap(findTestObject('Object Repository/Pengaturan Pengguna Page/button.TambahPengguna'), 0)

Mobile.setText(findTestObject('Object Repository/Pengaturan Pengguna Page/Tambah,Ubah,Delete Pengguna Page/editText.Username'),
	expectedUsername, 0)

Mobile.setText(findTestObject('Object Repository/Pengaturan Pengguna Page/Tambah,Ubah,Delete Pengguna Page/editText.Nama'), expectedNama,
	0)

Mobile.setText(findTestObject('Object Repository/Pengaturan Pengguna Page/Tambah,Ubah,Delete Pengguna Page/editText.PasswordBaru'),
	expectedPasswordBaru, 0)

Mobile.tap(findTestObject('Object Repository/Pengaturan Pengguna Page/Tambah,Ubah,Delete Pengguna Page/Spinner.Peran'), 0)

Mobile.tap(findTestObject('Object Repository/Pengaturan Pengguna Page/Tambah,Ubah,Delete Pengguna Page/spinner.PilihRole',
	[('role') : expectedPeran]), 0)

Mobile.tap(findTestObject('Object Repository/Pengaturan Pengguna Page/Tambah,Ubah,Delete Pengguna Page/button.PilihLokasi'), 0)

Mobile.setText(findTestObject('Object Repository/Pengaturan Pengguna Page/Tambah,Ubah,Delete Pengguna Page/Pilih Lokasi Page/edittext.CariLokasi'), expectedCariLokasi, 0)

Mobile.tap(findTestObject('Object Repository/Pengaturan Pengguna Page/Tambah,Ubah,Delete Pengguna Page/Pilih Lokasi Page/button.ListLokasi',['listLokasi':expectedCariLokasi]), 0)

Mobile.tap(findTestObject('Object Repository/Pengaturan Pengguna Page/Tambah,Ubah,Delete Pengguna Page/Pilih Lokasi Page/button.Pilih'), 0)

Mobile.tap(findTestObject('Object Repository/Pengaturan Pengguna Page/Tambah,Ubah,Delete Pengguna Page/compoundButton.Lobi',['lobi':expectedLobi]), 0)

Mobile.tap(findTestObject('Object Repository/Pengaturan Pengguna Page/Tambah,Ubah,Delete Pengguna Page/compoundButton.Pengendapan',['pengendapan':expectedPengendapan]), 0)

Mobile.tap(findTestObject('Object Repository/Pengaturan Pengguna Page/Tambah,Ubah,Delete Pengguna Page/button.Simpan'), 0)