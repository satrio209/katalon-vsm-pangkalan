import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.tap(findTestObject('Object Repository/Antrian Armada, Antrian Penumpang/Antrean Armada/button.TambahArmada'), 0)

def expectedNoTaxi = GlobalVariable.DONoTaxi

Mobile.setText(findTestObject('Object Repository/Antrian Armada, Antrian Penumpang/Antrean Armada/Tambah Armada Page/Edittext.MasukanNoTaksi'), 
    expectedNoTaxi, 0)

Mobile.tap(findTestObject('Object Repository/Antrian Armada, Antrian Penumpang/Antrean Armada/Tambah Armada Page/button.Notaxi', 
        [('noTaxi') : expectedNoTaxi]), 0)

Mobile.verifyElementVisible(findTestObject('Object Repository/Antrian Armada, Antrian Penumpang/Antrean Armada/Tambah Armada Page/Pop Up Gagal Tambahkan Fleet/textView.MenambahkanFleetgagal'), 
    0)

Mobile.verifyElementVisible(findTestObject('Object Repository/Antrian Armada, Antrian Penumpang/Antrean Armada/Tambah Armada Page/Pop Up Gagal Tambahkan Fleet/textView.UNKNOWN Duplicate fleet'), 
    0)

String Actual = Mobile.getText(findTestObject('Object Repository/Antrian Armada, Antrian Penumpang/Antrean Armada/Tambah Armada Page/Pop Up Gagal Tambahkan Fleet/textView.UNKNOWN Duplicate fleet'), 
    0)

String message = Mobile.verifyMatch(Actual, 'UNKNOWN: Duplicate fleet', false)

KeywordUtil.logInfo(message)

CustomKeywords.'mobile.screenshot.takeSS'()

Mobile.tap(findTestObject('Object Repository/Antrian Armada, Antrian Penumpang/Pop Up Lokasi Tidak Ditemukan/button.Kembali'), 
    0)

for (int i = 0; i < 2; i++) {
    Mobile.tap(findTestObject('Object Repository/_Global Object/button.Burger'), 0)
}