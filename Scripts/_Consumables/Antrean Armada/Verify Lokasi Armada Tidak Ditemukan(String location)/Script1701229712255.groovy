import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.tap(findTestObject('Object Repository/_Global Object/button.Burger'), 0)

Mobile.tap(findTestObject('Object Repository/Burger Menu/button.AntreanArmada'), 0)

Mobile.tap(findTestObject('Object Repository/Antrian Armada, Antrian Penumpang/button.Search'), 0)

String expectedResultLocation = GlobalVariable.DOLocation

Mobile.setText(findTestObject('Object Repository/Antrian Armada, Antrian Penumpang/Edittext.CariNamaLokasi'), expectedResultLocation, 
    GlobalVariable.DOTimeOut)

Mobile.verifyElementVisible(findTestObject('Object Repository/Antrian Armada, Antrian Penumpang/Pop Up Lokasi Tidak Ditemukan/textView.TidakDapatMenemukanLokasi'), 
    GlobalVariable.DOTimeOut)

Mobile.verifyElementVisible(findTestObject('Object Repository/Antrian Armada, Antrian Penumpang/Pop Up Lokasi Tidak Ditemukan/textView.NamaYangDicariTidakDitemukan'), 
    GlobalVariable.DOTimeOut)

Mobile.tap(findTestObject('Object Repository/Antrian Armada, Antrian Penumpang/Pop Up Lokasi Tidak Ditemukan/button.Kembali'), 
    GlobalVariable.DOTimeOut)