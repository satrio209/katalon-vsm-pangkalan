import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys


def noAntreanExpected = GlobalVariable.DOAntreanBaru

Mobile.tap(findTestObject('Object Repository/Antrian Armada, Antrian Penumpang/Antrean Penumpang/button.AmbilAntrean'), 
    0)

Mobile.verifyElementVisible(findTestObject('Object Repository/Antrian Armada, Antrian Penumpang/Antrean Penumpang/Pop Up Ambil No. Antrean/textView.NoAntrean', 
        [('no') : noAntreanExpected]), 0)

CustomKeywords.'mobile.swipe.SwipeRightAntrean'()

Mobile.verifyElementVisible(findTestObject('Object Repository/Antrian Armada, Antrian Penumpang/Antrean Penumpang/Pop Up Ambil No. Antrean/Tiket Antrian/textView.AntreanBerhasilDibuat'), 
    0)

Mobile.verifyElementVisible(findTestObject('Object Repository/Antrian Armada, Antrian Penumpang/Antrean Penumpang/Pop Up Ambil No. Antrean/Tiket Antrian/TextView.NoAntrean'), 
    0)

CustomKeywords.'mobile.screenshot.takeSS'()

String ActualResult = Mobile.getText(findTestObject('Object Repository/Antrian Armada, Antrian Penumpang/Antrean Penumpang/Pop Up Ambil No. Antrean/Tiket Antrian/textView.No'), 
    0)

Mobile.verifyMatch(ActualResult, noAntreanExpected, false)

KeywordUtil.logInfo('No. antrean' + ' ' + noAntreanExpected+ ' '+ 'telah ditambahkan')

Mobile.tap(findTestObject('Object Repository/Antrian Armada, Antrian Penumpang/Antrean Penumpang/Pop Up Ambil No. Antrean/Tiket Antrian/button.OK'), 
    0)

for (int i = 0; i < 1; i++) {
    Mobile.tap(findTestObject('Object Repository/_Global Object/button.Burger'), 0)
}