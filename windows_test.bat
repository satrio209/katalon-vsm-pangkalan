@echo off

@REM change your path of java v8
SET JAVA_HOME=C:\Program Files (x86)\Java\jdk1.8.0_74
SET PATH=%JAVA_HOME%/bin:$PATH

set PROJECT=%cd%\%1
echo %2
echo %3
echo %4
echo %PROJECT%
@REM PATH OF KATALON engine
C:\Users\Windows10\Downloads\Katalon_Studio_Engine_Windows_64-8.6.8\Katalon_Studio_Engine_Windows_64-8.6.8\katalonc -noSplash -runMode=console -projectPath="%PROJECT%" -retry=0 -testSuitePath="%2" -browserType="Android" -deviceId="%3" -executionProfile="staging" -apiKey="%4" --config -proxy.auth.option=NO_PROXY -proxy.system.option=NO_PROXY -proxy.system.applyToDesiredCapabilities=true

