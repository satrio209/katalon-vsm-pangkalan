<?xml version="1.0" encoding="UTF-8"?>
<MobileElementEntity>
   <description></description>
   <name>textView.BerhasilAddStock</name>
   <tag></tag>
   <elementGuidId>d9164c9d-a444-47a2-a7db-9ff555269c4d</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>${noTaxi} berhasil ditambah ke antrian</value>
      <webElementGuid>74f2a49f-790b-4cca-8a4c-8dee8ae49155</webElementGuid>
   </webElementProperties>
   <locator>//*[(@text = '${noTaxi} berhasil ditambah ke antrian' or . = '${noTaxi} berhasil ditambah ke antrian')]</locator>
   <locatorStrategy>XPATH</locatorStrategy>
</MobileElementEntity>
