<?xml version="1.0" encoding="UTF-8"?>
<MobileElementEntity>
   <description></description>
   <name>textView.SuccesAddStock</name>
   <tag></tag>
   <elementGuidId>8c49c491-2cd6-4d05-a512-973fadfc9cc7</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>${user} berhasil dibuat</value>
      <webElementGuid>9f249ec7-c0dd-4cbd-b126-e68d9d9fccdf</webElementGuid>
   </webElementProperties>
   <locator>//*[(@text = '${user} berhasil dibuat' or . = '${user} berhasil dibuat')]</locator>
   <locatorStrategy>XPATH</locatorStrategy>
</MobileElementEntity>
